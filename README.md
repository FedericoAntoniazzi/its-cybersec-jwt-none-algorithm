# Vulnerable JWT APIs

This is a vulnerable JWT API service for demostrating JWT none-algorithm attacks.

The service exposes 2 endpoints:
- `/signup`: Get a new JWT token
- `/admin`: Verify the token belongs to an admin

Remember to use `npm install` before executing `server.js`.

### Interacting with the APIs
1. Getting a token for a non-admin user
```sh
	curl -X POST localhost:3000/signup 
```
2. Use the token to access the admin page
```sh
	TOKEN="TOKEN-YOU-RECEIVED"
	curl -H "Accept: application/json" -H "Authorization: Bearer ${TOKEN}" http://localhost:3000/admin
```
3. The reply says the token does not belong to ad admin user
4. Generate a new token with `alg: "none"` and no signature
```
	node create-none.js
```
5. Use the malicious token to get admin access
```sh
	TOKEN="TOKEN-YOU-RECEIVED"
	curl -H "Accept: application/json" -H "Authorization: Bearer ${TOKEN}" http://localhost:3000/admin
```
6. You are an admin!
