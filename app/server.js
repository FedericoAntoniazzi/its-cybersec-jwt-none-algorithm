const fastify = require('fastify')({
	logger: "true"
})
const jwt = require('jsonwebtoken')
const jwtSecret = "Vmware1!"

fastify.post('/signup', (_, reply) => {
	payload = {
		"user": "Federico",
		"admin": false
	}

	const token = jwt.sign(payload, jwtSecret)
	reply.send({ token })
});

fastify.get('/admin', async (req, reply) => {
	const auth = req.headers.authorization;
	const token = auth.split(' ')[1]
	// Log token
	// req.log.info(token)

	let decodedToken = ""

	try {
		decodedToken = jwt.verify(token, jwtSecret, {algorithms: ['HS256']})
	} catch (err) {
		decodedToken = jwt.verify(token, '', {algorithms: ['none']})
	}
	// Log decoded token
	// req.log.info(decodedToken)

	// If the user is admin...
	if(decodedToken.admin !== undefined && decodedToken.admin === true) {
		// return "Attacco riuscito, sei un admin"
		reply.code(200).send("Ciao admin!")
	} else {
		// return "Accesso limitato solo agli admin"
		reply.code(401).send("Accesso limitato solo agli admin")
	}
});

fastify.listen(3000, err => {
	if (err) throw err
});
